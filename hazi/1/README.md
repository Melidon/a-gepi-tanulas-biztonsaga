# A gépi tanulás biztonsága (VIHIMB09) - 1. házi feladat

## Bálint Gergő [O78UXU], Szommer Zsombor [MM5NOT]

## 1. Feladat

A következő metrikákat használtuk a modell kiértékelésére:

```python
from sklearn.metrics import accuracy_score, confusion_matrix, roc_auc_score
```

### 1.1. Mi a betanított modell átlagos pontossága a teszt adaton ha s=2^14?

A betanított modell átlagos pontossága a teszt adaton a következő:

```
Accuracy: 0.9846153846153847
```

Ez az egyhez nagyon közel álló érték azt jelenti, hogy a modell pontossága közel tökéletes az adathalmazon.

### 1.2. Mi a modell TPR, TNR, FPR, FNR, valamint AUC értéke a teszt adaton? Mi állapítható meg ezekből a modell teljesítményéről?

A betanított modellt a teszt adathalmazon kiértékelve a következő eredményeket kaptuk:

```
True Positive Rate (TPR): 0.9743589743589743
True Negative Rate (TNR): 0.9948717948717949
False Positive Rate (FPR): 0.005128205128205128
False Negative Rate (FNR): 0.02564102564102564

Area Under the ROC Curve (AUC): 0.995318869165023
```

Az értékek alapján elmondható, hogy a modell nagyon jól teljesít, mivel a TPR és TNR értékek nagyon közel vannak az 1-hez, az FPR és FNR értékek pedig alacsonyak, ami azt jelenti, hogy a modell szinte egyáltalán nem téveszt.
Az AUC érték is közel van az 1-hez, ami azt jelenti, hogy a modell nagyon jól teljesít, döntése magabiztos.
Ez a mutató jobb az accuracy-nél, mivel ez nem függ a döntési határértéktől, emiatt stabilabb teljesítménymutató.

## 2. Feladat

### 2.1. Először értékeljen ki egy egyszerű baseline támadást, ahol a támadó random byte értékekre állítja az adversarial suffix-et és ezt fűzi hozzá a binárishoz (tehát nem használ PGD-t). Számolja ki a támadás pontosságát (a sikeresen félreklasszifikált malware minták hányada a  victim/malware  könyvtárban) ha az adversarial suffix mérete az eredeti bináris fájl hosszának 5, 10, 15 és 20%-a! Mindegyik esetben adja meg az adversarial maszk |M| méretét is!

### 2.2. Számolja ki a PGD támadás pontosságát (a sikeresen félreklasszifikált malware minták hányada a victim/malware  könyvtárban) ha az adversarial suffix mérete az eredeti bináris fájl hosszának 5, 10, 15 és 20%-a! Mindegyik esetben adja meg az adversarial maszk |M| méretét is!

A feladatok megoldásainak eredményeit egy nagy táblázatban foglaljuk össze.
Ez tartalmazza a megadott suffix arányokra bontva soronkén véve egy-egy malware mintát: az adversarial maszk |M| méretét, a random és a PGD támadások sikerességét.
Az egyes támadások sikeresség-aránya az adversarial suffix méretének függvényében a táblázat legalsó sorában láthatók.

| \|M\| 5% | RAND 5%  |  PGD 5%  | \|M\| 10% | RAND 10% | PGD 10%  | \|M\| 15% | RAND 15% | PGD 15%  | \|M\| 20% | RAND 20% | PGD 20%  |
| :------: | :------: | :------: | :-------: | :------: | :------: | :-------: | :------: | :------: | :-------: | :------: | :------: |
|   772    |   fail   | success  |   1473    |   fail   | success  |   2112    |   fail   | success  |   2696    | success  | success  |
|   779    |   fail   | success  |   1467    |   fail   | success  |   2118    |   fail   | success  |   2721    |   fail   | success  |
|   718    |   fail   | success  |   1436    |   fail   | success  |   1938    |   fail   | success  |   2585    |   fail   | success  |
|   727    |   fail   | success  |   1454    |   fail   | success  |   1745    |   fail   | success  |   2327    |   fail   | success  |
|   632    |   fail   |   fail   |   1264    |   fail   |   fail   |   1896    |   fail   | success  |   2528    |   fail   | success  |
|   727    |   fail   | success  |   1454    |   fail   | success  |   2026    |   fail   | success  |   2701    |   fail   | success  |
|   751    |   fail   | success  |   1335    |   fail   | success  |   2004    |   fail   | success  |   2672    |   fail   | success  |
|   765    |   fail   | success  |   1404    |   fail   | success  |   2106    |   fail   | success  |   2593    |   fail   | success  |
|   711    |   fail   | success  |   1422    |   fail   | success  |   2133    |   fail   | success  |   2370    |   fail   | success  |
|   664    |   fail   |   fail   |   1329    |   fail   | success  |   1994    |   fail   | success  |   2658    |   fail   | success  |
|   691    |   fail   | success  |   1383    |   fail   | success  |   2076    |   fail   | success  |   2372    |   fail   | success  |
|   728    |   fail   | success  |   1457    |   fail   | success  |   1912    |   fail   | success  |   2550    |   fail   | success  |
|   723    |   fail   | success  |   1447    |   fail   | success  |   1973    | success  | success  |   2631    | success  | success  |
|   732    |   fail   | success  |   1465    |   fail   | success  |   1885    | success  | success  |   2514    | success  | success  |
|   697    | success  | success  |   1395    |   fail   | success  |   2093    | success  | success  |   2480    | success  | success  |
|   725    |   fail   | success  |   1450    |   fail   | success  |   1933    | success  | success  |   2578    |   fail   | success  |
|   680    |   fail   |   fail   |   1361    |   fail   | success  |   2042    |   fail   | success  |   2723    |   fail   | success  |
|   568    |   fail   |   fail   |   1136    |   fail   |   fail   |   1705    |   fail   | success  |   2273    |   fail   | success  |
|   761    |   fail   | success  |   1354    |   fail   | success  |   2032    |   fail   | success  |   2710    |   fail   | success  |
|   574    |   fail   |   fail   |   1149    |   fail   |   fail   |   1724    |   fail   |   fail   |   2299    |   fail   | success  |
|   628    |   fail   |   fail   |   1257    |   fail   |   fail   |   1886    |   fail   | success  |   2515    |   fail   | success  |
|   759    |   fail   | success  |   1349    |   fail   | success  |   2024    |   fail   | success  |   2699    |   fail   | success  |
|   628    |   fail   |   fail   |   1257    |   fail   |   fail   |   1886    |   fail   | success  |   2515    |   fail   | success  |
|   744    |   fail   | success  |   1356    |   fail   | success  |   2034    |   fail   | success  |   2712    |   fail   | success  |
|   729    |   fail   | success  |   1458    |   fail   | success  |   1640    |   fail   | success  |   2187    |   fail   | success  |
|   723    |   fail   | success  |   1447    |   fail   | success  |   1973    | success  | success  |   2631    | success  | success  |
|   693    |   fail   | success  |   1386    |   fail   | success  |   2079    |   fail   | success  |   2218    |   fail   | success  |
|   609    |   fail   | success  |   1219    |   fail   | success  |   1829    |   fail   | success  |   2439    |   fail   | success  |
|    24    |   fail   |   fail   |    48     |   fail   |   fail   |    72     |   fail   |   fail   |    96     |   fail   |   fail   |
|   776    |   fail   | success  |   1379    |   fail   | success  |   2069    |   fail   | success  |   2483    |   fail   | success  |
|   707    |   fail   | success  |   1415    |   fail   | success  |   2123    |   fail   | success  |   2476    | success  | success  |
|   691    |   fail   | success  |   1383    |   fail   | success  |   2076    |   fail   | success  |   2372    |   fail   | success  |
|   666    |   fail   |   fail   |   1333    |   fail   | success  |   2000    |   fail   | success  |   2667    |   fail   | success  |
|   614    |   fail   | success  |   1228    | success  | success  |   1842    |   fail   | success  |   2457    |   fail   | success  |
|   771    |   fail   | success  |   1414    |   fail   | success  |   2121    |   fail   | success  |   2611    |   fail   | success  |
|   779    |   fail   | success  |   1467    |   fail   | success  |   2118    |   fail   | success  |   2721    |   fail   | success  |
|   750    |   fail   | success  |   1250    |   fail   | success  |   1876    |   fail   | success  |   2501    |   fail   | success  |
|   755    |   fail   | success  |   1359    |   fail   | success  |   2040    |   fail   | success  |   2720    |   fail   | success  |
|   709    |   fail   | success  |   1419    |   fail   | success  |   2130    |   fail   | success  |   2435    | success  | success  |
|   634    | success  | success  |   1268    | success  | success  |   1903    | success  | success  |   2538    | success  | success  |
|   704    | success  | success  |   1410    | success  | success  |   2115    | success  | success  |   2116    | success  | success  |
|   728    |   fail   | success  |   1457    |   fail   | success  |   1912    |   fail   | success  |   2550    |   fail   | success  |
|   711    |   fail   | success  |   1423    |   fail   | success  |   2134    |   fail   | success  |   2561    |   fail   | success  |
|   617    |   fail   |   fail   |   1235    |   fail   | success  |   1853    |   fail   | success  |   2471    |   fail   | success  |
|   727    |   fail   | success  |   1455    |   fail   | success  |   2026    |   fail   | success  |   2702    |   fail   | success  |
|   646    |   fail   |   fail   |   1292    |   fail   | success  |   1938    |   fail   | success  |   2584    |   fail   | success  |
|   745    |   fail   | success  |   1376    |   fail   | success  |   2064    |   fail   | success  |   2556    |   fail   | success  |
|   758    |   fail   | success  |   1365    |   fail   | success  |   2048    |   fail   | success  |   2483    | success  | success  |
|   617    |   fail   |   fail   |   1235    |   fail   | success  |   1853    |   fail   | success  |   2471    |   fail   | success  |
|   727    |   fail   | success  |   1454    |   fail   | success  |   1745    |   fail   | success  |   2327    |   fail   | success  |
|          |          |          |           |          |          |           |          |          |           |          |          |
|          | **0.06** | **0.76** |           | **0.06** | **0.88** |           | **0.14** | **0.96** |           | **0.2**  | **0.98** |

A táblázat adatai alapján kijelenthető, hogy a PGD támadás sokkal hatékonyabb, mint a random támadás.
A PGD támadás sikeressége az adversarial suffix méretének növekedésével egyre nő, míg a random támadás sikeressége nem változik számottevően.
A PGD támadás sikeressége az adversarial suffix méretének 5%-os növekedésével is már 76%-os, míg a random támadás sikeressége 20%-os suffix méret esetén is csak 20%-os.
Emellett, ha a file mérete eredetileg kicsi, akkor |M| is kicsi, ennek eredménye nagy valószínűséggel a támadás sikertelensége.

### 2.3. Utólagos feladat

A Teams-re kiírt feladatváltoztatás alapján kiszámoltuk a maszk eredeti file-mérethez képesti relatív hosszának átlagát az adott Suffix százalékos estekre. Az eredmények a következők:

|        S 5%         |        S 10%        |        S 15%        |        S 20%        |
| :-----------------: | :-----------------: | :-----------------: | :-----------------: |
| 0.04994745359010151 | 0.09994128765839669 | 0.14994159442172844 | 0.19994062810495492 |

Megfigyelhető, hogy a maszk majdnem pontosan akkora, mint amennyivel a file méretét növeltük. Minél hosszabb az eredeti file, ez az arány annál jobban közelíti az S növekményt.
