
# Beadási határidő: 2024.05.12. 23:59 
## (késedelmes beadás határideje: 2024.05.19. 23:59)

A házi feladat két részből áll. Először az első házi feladatban megvalósított malware detektor ellen kell nem célzott adatszennyezést végrehajtani (untargeted data poisoning), majd a második részben célzott adatszennyezést (targeted poisoning). A megoldás során a csatolt Python Notebook-ot kell kiegészíteni. 

# Támadó modell

A támadó hozzáfér a teszt és victim adatokhoz, az első feladatban pedig a tanítóadathoz is. Továbbá a támadó ismeri a malware detektor hyper-paramétereit (pl. a háló architektúráját, optimalizációs algoritmus, stb.) de a megtámadott modell paramétereit nem. A támadás során képes fabrikált mintákat hozzáadni a tanítóadathoz, de a már benne levőket nem tudja módosítani.

# Előkészítés

Mindkét feladathoz szükséges öt különböző surrogate modell betanítása a **tesztadaton** az első házi feladatban leírt módon (mindegyik teljes újratanítás véletlen inicializált model paraméterekkel és véletlen mini-batch sorrenddel). A támadó hozzáfér ezekhez a betanított surrogate modellekhez és felhasználhatja a támadásokhoz.

# 1. feladat

A támadó célja, hogy a malware detektor pontosságát lerontsa a tesztadaton:  veszi a tanítási adat $p$ %-át, ezek címkéit invertálja (malware-ről nem malware-re, nem malware-ről malware-re), majd az átcímkézett adatot hozzáadja a tanítási adathoz. Az így kibővített adaton kell újratanítani a modellt, és meghatározni a kapott modell pontosságát a tesztadaton.

## Kérdések

1. A támadó véletlenül választja az átcímkézendő mintákat az összes tanítóadat közül. Mi lesz a beszennyezett adaton tanított modell pontossága ha $p$ értéke 30, 50 és 70%? Minden esetben értékelje ki a modell átlagos pontosságát legalább 5 különböző tanításon, és ábrázolja box-ploton az eredményeket! 
2. A támadó a legnagyobb loss értékű mintákat választja átcímkézésre az összes tanítóadat közül. Ehhez minden tanító minta loss értékét kiszámolja a minta invertált címkéjére nézve az előkészítésben kiszámolt 5 surrogate modell mindegyikén. Minden mintára az 5 darab loss értéket átlagolja, majd kiválasztja a legnagyobb átlagos loss értékű minták felső $p$ %-át, és hozzáadja ezeket invertált címkével a tanítóadathoz. Mi lesz a beszennyezett adaton betanított modell pontossága ha $p$ értéke 30, 50 és 70%? Minden esetben értékelje ki a modell átlagos pontosságát legalább 5 különböző tanításon, és ábrázolja box-ploton az eredményeket!
3. Hasonlítsa össze a fenti két esetben kapott eredményeket! Melyik hatékonyabb és miért?
4. Elemezze a támadások megvalósíthatóságát a gyakorlatban! Javasoljon bármilyen védekezést amivel a támadás sikeressége csökkenthető (nem kell implementálni)!

# 2. feladat

A feladat egy célzott adatszennyezéses támadás megvalósítása. A félreklasszifikálandó (target) binárisok nevei a `malware_victims.json`  és `benign_victims.json` fájlokban vannak felsorolva, amely binárisok rendre a `data/victim/malware/` és `data/victim/benign/` mappákban találhatók (ld. első házi feladat). A cél, hogy ezt az összesen 10 darab binárist egy szennyezett adaton betanított modell félreklasszifikálja deployment után a tesztfázisban. A támadó ehhez kiválaszt $p$ darab binárist a teszthalmazból (`data/test/`), amelyek címkéje megegyezik azzal az $y_t^{adv}$ osztállyal ahova félre akar klasszifikálni, és módosítja a binárisból képzett modell-bemenetet (ami egy $2^{14}$ hosszú vektor az első házi feladat szerint) úgy, hogy az így kapott mintán számolt gradiens illeszkedni fog a $y_t^{adv}$ címkével ellátott target mintán számolt gradienssel. Vagyis a hozzáadott poison minták "szimulálják" az $y_t^{adv}$ osztályra átcímkézett target minta gradiensét a tanítás során, mintha az benne lenne a tanítóadatban. A WiB támadás során az alábbi alignment loss értéket kell minimalizálni $\delta_i$ szerint ($1\leq j \leq p$) adott $x_t$ target mintára és $y_t^{adv}$ választott osztályra:
$$\min_{\{\delta_1, \ldots, \delta_p\}} \frac{1}{5}\sum_{j=1}^5 \sum_{(x_i,y_i)\in D_t'} (1 - \cos\angle(\nabla_\theta loss_f(\theta_j, x_t, y_t^{adv}), \nabla_\theta loss_f(\theta_j, x_i + \delta_i, y_i)))$$ ahol $(x_t, y_t)$ a target minta és eredeti címke, $y_t^{adv}=\overline{y_t}=y_i$ a támadó által választott célosztály, $\{\theta_1, \ldots, \theta_5\}$ az előkészítés során betanított 5 darab surrogate modell amivel a támadó rendelkezik, $D_t'\subseteq D_{test}$ a támadó által választott teszt (base) minták halmaza ($|D_t'|=p$), $\delta_i$ pedig az $i$. base mintához tartozó perturbáció (módosítás) ami egy irányba állítja a target és base minták gradiensét. A $D_t'$ base mintákat úgy választja, hogy azok gradiense és a target minta gradiense eleve a leginkább hasonlóak legyenek az összes teszt minta közül: Minden tesztminta és adott  $(x_t, y_t^{adv})$ target minta gradiensének a koszinusz hasonlóságát kiszámolja a $\{\theta_1, \ldots, \theta_5\}$ modellek mindegyikén, ezt az 5 darab hasonlósági értéket átlagolja minden tesztminta esetén, majd kiválasztja a $p$ legnagyobb átlagos koszinusz hasonlósággal rendelkező tesztmintákat. A támadás kimenete a $\{(x_i+\delta_i, y_i)\}_{i=1}^p$ poison minták halmaza, amit a tanítóadathoz kell adni, és a modellt az így szennyezett adaton újratanítani. 

További részletekért lásd a Witches' Brew (WiB) támadást a 6. előadás anyagában. 
(Megjegyzés: Az előadásban a  $\frac{1}{5}\sum_{j=1}^5 (1 - cos\angle(\nabla_\theta loss_f(\theta_j, x_t, y_t^{adv}),  \sum_{(x_i,y_i)\in D_t'}\nabla_\theta loss_f(\theta_j, x_i + \delta_i, y_i)))$ formula szerepelt, amit ugyan gyorsabb kiszámolni de pontatlanabb támadást eredményez) 

# Kérdések:
1. Hajtsa végre a fenti támadást minden egyes target binárisra ha $p\in\{5, 10, 15\}$ (ez 10 darab támadás minden $p$ értékre)! Számolja ki a támadás átlagos pontosságát minden egyes $p$ értékre, vagyis a sikeresen félreklasszifikált target minták arányát! A támadás akkor sikeres, ha a szennyezett tanítóadaton újratanított modell (random inicializálva véletlen mini-batch választás esetén) az $x_t$ bemenetre az $y_t^{adv}$ kimenetet adja.
2. Elemezze a támadás megvalósíthatóságát a gyakorlatban! Javasoljon bármilyen védekezést amivel a támadás sikeressége csökkenthető (nem kell implementálni)!

*Megjegyzések:* 
- Figyeljen arra, hogy a perturbáció érvényes bájt értéket adjon (0 és 1 között skálázott modell input esetén, vagy 0 és 255 nem skálázott input esetén)!
- A base mintákból számolt input vektor minden koordinátáját módosíthatja a támadó  (de az eredeti $y_i$ címkét nem)!
- A perturbált $x_i + \delta_i$ base mintákból nem kell egy valid binárist generálni és azokon tanítani, a módosított input vektorok használhatók közvetlenül tanításhoz! 
- A $loss_{align}$ minimalizálását érdemes PGD-vel végezni (pl. SGD optimalizációval), dinamikusan csökkenő learning rate értékkel. Pl: `ReduceLROnPlateau(optimizer, 'min', patience=75, eps=1e-06, verbose=True)` használata estén nem szükséges 1000-nél több PGD iteráció. Ha az 1000. iteráció után kapott poison minták sem eredményeznek félreklasszifikálást, akkor a támadás tekinthető sikertelennek.
	
# Beadás módja

Egyetlen ZIP-fájlt kell létrehozni és a Moodle-be feltölteni, amelynek neve: `mlsec_hw_2_<NEPTUN_ID1>-<NEPTUN_ID2>.zip` (cserélje le `<NEPTUN_ID>`-t a saját és csapattagja neptun azonosítójára), amely tartalmazza a kiegészített Python Notebook-ot és ugyanebben a notebook-ban a kérdésekre adott válaszokat. 

A forráskódot legyen kommentezve, adatot (binárist, numpy tömböket stb.) nem kell feltölteni!