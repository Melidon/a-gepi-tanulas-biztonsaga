import torch.nn as nn
import torch
import numpy as np
import math

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Sequential(         
            nn.Conv1d(
                in_channels=1,              
                out_channels=16,            
                kernel_size=10,              
                stride=1,                   
                padding=0,                  
            ),                              
            nn.ReLU(),                      
            nn.MaxPool1d(kernel_size=4),    
        )
        self.out = nn.Linear(65488, 1)


    def forward(self, x):
        x = self.conv1(x)
        # flatten the output of conv1
        x = x.view(x.size(0), -1)       
        return self.out(x)

# resize a byte vector to a given size and scale it
# if the vector is smaller, it is padded with zero
# if the vector is larger, it is averaged
# resize([1, 2, 3, 4], 2, 1) -> [1.5, 3.5]
# resize([1, 2, 3, 4], 3, 1) -> [1.5 3.5 0. ]
# resize([1, 2, 3, 4], 6, 1) ->  [1. 2. 3. 4. 0. 0.]
def resize(binary, size = 2**14, scale = 255.0):
    if size == binary.size:
        ret = binary
    
    elif size > binary.size:
        ret = np.append(binary, np.zeros(size - binary.size) * np.NaN)
    
    else:
        R = int(math.ceil(binary.size / size))
        pad_size = int(math.ceil(binary.size / R)) * R - binary.size 
        b_padded = np.append(binary, np.zeros(pad_size) * np.NaN)
        tmp = np.nanmean(b_padded.reshape(-1, R), axis=1)
        ret = np.append(tmp, np.zeros(size - tmp.size) * np.NaN)

    return np.nan_to_num(ret) / scale

