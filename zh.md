# Biztonság a gépi tanulásban ZH (VIHIMB09)

2024\. április 18.

1.  Mi a különbség a poisioning és az evasion támadások között?
    Mi a támadó modell a két támagás esetén?
    (4 pont)

2.  Hogyan működik a Fast Gradient Sign Method (FGSM)?
    Mi az előnye és a hátránya a többi white-box támadáshoz képest?
    (4 pont)

3.  Evasion támadás esetén a támadónak ki kell számolnia a loss függvény gradiensét.
    Hogyan lehet hatékonyan megbecsülni a gradienst mintavételezéssel black-box támadás esetén, ha a loss függvény cross-entropy?
    (5 pont)

4.  Hogyan működik a feature collision alapú célzott poisioning támadás?
    Mi a támadás célja és feltételei (támadó modellje)?
    (5 pont)

5.  k-nearest-neighbor alapú védekezést használunk feature collision ellen.
    Tudjuk, hogy a támadó legfeljebb 11 poision mintát szúrhat be célja elérése érdekében.
    Hogy kell beállítani k értékét, hogy a támadás garantáltan ne érje el a célját?
    Miért?
    (4 pont)

6.  Formalizálja a robusztus tanulás célját evasion támadás esetén!
    Gyakorlatban hogyan lehet megvalósítani egy ilyen robosztus tanulást?
    (4 pont)
