# Intro

## CIA

- Confidentiality: A támadó a tanítóadatról, vagy modellről szeretne információt szerezni.
- Integrity: Rossz kimenetet szeretne elérni a támadó.
- Availability: A modell teljesítményét akarja rontani a támadó.

### Confidentiality

Membership attack: Adott egy tanító minta. Meg lehet-e állapítani, hogy használták-e a modell tanításához?
Model stealing: Model extraction: A meglévő modellt orákulumnak használva felcímkézünk egy tanító adathalmazt, amint utána saját modellt tanítunk.

### Integrity

Lehet evasion vagy poisoning.

A kettő közötti különbséget összehasonlító ábrán nem értem, hogy az evasion-nál miért a modell van bekarikázva.
Mármint ha a poisioning-nél a minták voltak bekarikázva, akkor az evasion-nál, ahol konkrétan "adversary examle" gyártása a cél, ott miért nem?

### Availability

Sponge samples: A bemeneti minták arra vannak készítve, hogy maximalizálják az energiafogyasztást és a késleltetést.

## EU AI Act

Risk categories:

- Minimal risk
- Limited risk
- High risk
- Unacceptable risk

# Evasion: White-box

A támadások fajtái:

- Untargeted: Az X bemenethez egy olyan minimális perturbációt keresünk, amely a modell Y kimenetét bármi másra megváltoztatja.
- Targeted: Az X bemenethez egy olyan minimális perturbációt keresünk, amely a modell Y kimenetét egy adott cél osztályra változtatja.

A perturbációk lehetnek:

- L0 norma: A perturbációban lévő elemek száma.
- L1 norma: Az abszolút értékek összege.
- L2 norma: Az euklideszi távolság.
- Linfinity norma: Az elemek maximális eltérésének mértéke.

Formalizáció:

- Untargeted: Maximalizálni szeretnénk a hiba méretét a valós Y címkékre nézve.
- Targeted: Minimizálni szeretnénk a hiba méretét a cél Y címkére nézve.

Mindkét esetben a perturbációk a megengedett Lp norma alatt kell legyenek.

Boundness:

- Norm-bounded: A félreosztályozást szeretnénk elérni azzal a feltétellel, hogy a perturbáció a megengedett Lp norma alatt legyen.
- Norm-unbounded: A perturbációt szeretnénk minimalizálni úgy, hogy a félreosztályozás a feltétel.

## Fast Gradient Sign Method (FGSM)

Norm-bounded.

A lépések sorban:
- Kiszámítja a loss-t.
- A bemenet szerint veszi a gradiensét.
- Veszi a gradiens szignumát.
- Ezt megszorozza epsilonnal.
- Így kapjuk a perturbációz, amit hozzá kell adni a bemenethez.

Untargeted esetben a loss-t maximalizáljuk az eredeti címke szerint.
Targeted esetben a loss-t minimalizáljuk a cél címke szerint.

A Hölder-egyenlőtlenséges működéséből nem értek semmit sem.

## Basic Iterative Method (BIM)

Norm-bounded.

Ugyanazt csinálja mint az FGSM, csak iteratívan.
Tehát:
- Kiindul egy alap nulla perturbációból.
- Kiszámítja a loss-t a bemenet + perturbációra.
- Veszi a loss gradiensét.
- Veszi a gradiens szignumát.
- Az eredményt megszorozza epsilonnal.
- Ezt hozzáadja a perturbációhoz.
- Visszavágja a perturbációt a megengedett Lp norma alá.

## Projected Gradient Descent (PGD) Attack

Norm-bounded.

Ugyanaz mint a BIM, csak itt nem egy teljesen nulla perturbációból indulunk ki, hanem egy véletlen perturbációból (de még mindig a megengedett Lp normán belül).
Sőt, igazából több véletlen perturbációból indulunk ki, és azok közül választjuk ki azt, amelyik a legnagyobb hibát okozza (vagy targeted esetnél a legkisebbet a cél címkére).

A Hölder-egyenlőtlenséges működéséből itt sem értek semmit.

## L-BFGS attack

Norm-unbounded.

Minimalizálni szeretnénk a perturbációt úgy, hogy a féreosztályozás a feltétel.

A matekjából már a formalizációt sem értem.

Viszont kevesebb perturbációt ad mint a BIM, csak sajnos lassú.

## DeepFool Attack

Norm-unbounded.

A döntési felületet közelítjük egy lineáris függvénnyel.
A perturbációt a döntési felületre merőlegesen növeljük.

De, hogy ezt miképpen csináljuk, arról fogalmam sincsen.

Amúgy viszont elvileg egy tök jó támadás, mert kevesebb perturbációt ad mint a BIM, és még gyors is.

## Carlini-Wagner (CW) Attack

Norm-unbounded.

Semmit sem értek belőle.

De állítólag ez az egyik legjobb támadás, mert nagyon kicsi perturbációval is el lehet érni a félreosztályozást és sok védekezés nem hatékony ellene.
Az egyetlen óriási baj vele, hogy nagyon lassú.

## Jacobian-based Saliency Map Attack (JSMA)

Norm-unbounded.

Nem lesz kérdezve vizsgán.
Ami tök jó, mert semmit sem értek belőle.

# Evasion: Black-box

**Transferability of adversarial examples**: Sok támadó minta amit egy adott modell ellen készítettünk működik egy másik modell ellen is, még akkor is ha mások a tanítóadatok, a hyperparaméterek vagy akár a modell architektúrája is.

## Transfer-based attack

Hasonló adatunk van mint amin a modell tanult.

1. A támadó lekérdezéseket intéz a támadni kívánt modellhez, hogy készísen róla egy saját replikát.
2. A támadó készíte egy támadó mintát a saját modelljén.
3. A támadó megtámadja a támadó mintával a támadni kívánt modellt.

## Score-based attacks

Tudjuk, hogy a modell mire milyen valószínűséget ad.

### simBA

Sajnos már nem kérdezik, pedig ez egész érthető volt.

- Fogjunk az X bemenetet.
- Fogunk egy random irányt egy egymásra ortogonális keresési irány halmazból.
- Ha az eredeti osztály valószínűsége csökken, akkor lépünk, ha nem, akkor maradunk.
- Ezt ismételgetjük mindefélre random irányokra.

### Gradient estimation

Azért szeretnénk becsülni a gradienst, hogy utána PGD-vel támadhassunk.

Sokkal pontosabb becslést kapunk a gradiensre, ha jobbra és balra is lépünk.

#### Naiv támadás

A bázis minden lehetséges egységvektorának irányában kiszámoljuk a gradienst, majd ezt összegezzük.

Az a baj vele, hogy túl lassú, mert egy képnek elképesztően sok pixele van.

#### Projekció

PCA-val kiválasztjuk a legfontosabb irányokat, majd csak ezek mentén számoljuk a gradienst.

Az a baj vele, hogy hasonló adattal kell rendelkeznünk, mint amint a modelltt tanították.

Amúgy a diasoron itt nem értem az ábrát.
Nem pont fordítva kéne lennie, hogy mi "safe" és mi "unsafe"?

#### Mintavételezés

Normál eloszlásból mintavételezgetünk random irányokat és ezek mentén számoljuk a gradienst.

A matekos diasor kegyetlen hozzá, nem értek semmit sem.
De amúgy nem is értem, minek kell ide a matekozás.
Úgy intuitívan tök jól hangzik, hogy ez majd működni fog.
Aztán mivel ki lett próbálva és valóban jól becsli a gradienst, ezért használjuk és kész.

## Decision-based attacks

Csak azt tudjuk, hogy a modell melyik osztályt választotta.

### Limited Attack

Feltételezzük, hogy a k legvalószínűbb osztályt tudjuk, de a pontos valószínűségeket nem, csak a sorrendjüket.
Az R értéket úgy számoljuk ki a, hogy kivonjuk a célosztály sorszámát k-ból.
Egy csomó random perturbációt generálunk, és azokra kiátlagoljuk az R értékeket.
Így kapunk egy S értéket, ami valamire biztosan jó.

### HopSkipJump

Nem teljesen értem, hogy mi az az F függvény, meg az x*, de az algoritmus legalább egyszerű.

Cél: Egy olyan pontot találni a döntési határon, amelyik a lehető legközelebb van a célhoz.

Algoritmus:
- Bináris kereséssel megkeresem a kiindulási pont és a célpont egyenesén a döntési határt.
- Meghatározom a döntési határra merőleges gradienst.
- A gradiens irányába lépek egyet úgy, hogy ne lépjek véletlen túl (azaz mindenképp a döntési határ azon oldalán maradjak, ahol a kiindulási pont is volt). Így kapom az új kiindulási pontot.
- Az egészet T-szer megismétlem.

Eredmény: Egy olyan pont a döntési határon, ami a lehető legközelebb van a célhoz.

Fogalmam sincsen, hogy ezt a pontot utána mire fogom használni.

Mindenesetre a támadás elvileg nagyon hatékony, mert kevés lekérdezés kell hozzá.

### OPT, signOPT

A kimeneti függvény nem deriválható, de lehet csinálni egy helyettesítő függvényt, ami deriválható és jól közelíti az eredetit.
Ez lesz a g függyvény, ami állítólag (=fogalmam sincsen, hogy miért) azt adja vissza, hogy milyen messze vagyunk a döntési határtól egy adott teta irányba.
Célunk, hogy megkeressük azt a tetát, ami a legkisebb távolságot adja.
Ezt gradiens süllyedéssel tesszük meg (valahogy).

A signOPT annyiban más, hogy ott máshogy van becsülve a g függvény teta szerinti deriváltja, ami miatt állítólag kevesebb lekérdezés kell.
Szerencsére ezt már nem kell tudni.

### Jacobian-based Dataset Augmentation

Semmit nem értettem belőle se előadáson és se most így utólag.
Mázli, hogy ezt nem fogják kérdezni.

# Evasion: Defenses

## Ad hoc defenses

Hamis biztonságérzetet adnak, valódi biztonságot nem.

### Gradient masking

A támadásoknak szükségük van a gradiensre.
Azt szeretnénk, hogy a támadó minták gyártásához minél haszontalanabb gadienst adjon a modell.

#### Shattered gradients

Nem differeciálható függvényeket is pakolunk a hálóba.

BPDA-val megtörhető.
Ennek lényege, hogy közelítjük valamivel ami differenciálható.

#### Stochastic gradients: Véletlenszerűséget vezetünk be a hálóba. Például dropoutot a kiértékelés alatt.

EOT-val megtörhető.
Ennek lényege, hogy átlagot számolunk a transzformációkból.

#### Exploding & Vanishing Gradients

Ezt elvileg nem kell tudni vizsgára.

### Támadó minták detektálása

Ötlet: Vegyük észre a támadó mintákat a logit értékeik alapján.

Baj: Megtörhető feature szintű támadó minták gyártásával.

### Ensemble Diversity

Több modellt tanítunk, és azok átlagát vesszük.
Ezt is meg lehet támadni átlagolással.

## Robust learning

Nem csak simán tanítunk, hanem tanítás közben készítünk támadásokat is, és azokon is tanuítjuk a modellt.
A lényeg tehát, hogy nem csak simán a loss-t minimalizáljuk, hanem a perturbációkkal elérhetől maximális loss-t próbáljuk minimalizálni.

Kétféle megközelítés van:

- lower bound: Keresünk valamilyen támadással (például PGD) egy olyan perturbációt, amelyik jó nagy hibát okoz.
- upper bound: Ténylegesen megkeressük a lehető legnaagyobb hibát okozó perturbációt. Ez nagyon lassú, de cserébe garantált.

## Certified Defenses

### Verifiable robustness

Formális verifikáció kéne, de az a baj, hogy az egy NP-nehéz probléma.
Szóval közelítéseket használunk, amik előfordulhatnak, hogy false positive-t adnak.

Utána van valami matek rész, hogy $Z_K=h_K(Z_{K-1})$, meg, hogy $(e_y-e_{y_{true}})^TZ_K\leq0$, amiből oké, hogy a $Z_K$ a modellt jelenti, meg az $e_y$ egy olyan egységvektort, aminek csak az y-dik koordinátája 1, a többi 0, de én ez képtelen vagyok kötni bármihez.
Mármint még annyi átjött, hogy ha nem áll fenn az egyenlőtlenség, akkor baj van, egyébként meg minden rendben, de akkor sem tudok vele mit kezdeni.
Mit hova kéne helyettesíteni ha tesztelni akarom a modellemet?

### Random smoothing

A kiértékelés során a bemenetre rárakunk egy random zajt.
Minél több a zaj, annál robusztusabb lesz a modell, de annál kevésbé lesz pontos.

A tanításhoz is kell, hogy a tanítóadatok egy része is zajos legyen, különben nem fog működni.

Nagy felbontású képek esetén sokkal jobban működik.

A matekot nem értem, de reméljük, hogy nem kérdeznek bele.

# Poisoning: Untargeted

## Label-flipping

Előnye, hogy nagyon egyszerű és nagyon hatékony.

Hátránya, hogy nagyon könnyen detektálható.

### Bi-level optimization

Még jobb lesz tőle a támadás.

De ez egy NP-nehéz probléma, szóval csak közelítésekkel tudjuk megoldani.

A formalizációt még értem, az algoritmust magát már nem.

## Optimal poisoning

Már ott elakadtam, hogy mit jelent egyáltalán azt, hogy egy modell konvex vagy nem konvex.

### Convex target model

Végtelen matek amiből semmit sem értek.

De a lényeg talán annyi, hogy össze lehet vonni a bi-level optimalizációra adott approximációs algoritmus két lépését, és ettől sokkal gyorsabb lesz.

### Non-convex target model

Valami derivált számítás nagyon drága.
Úgyhogy relaxációkat vezetünk be.

És itt jön végtelen matek amiből megint nem értek semmit.

## Secure Dataset Release with Poisoning

Ez nem lesz számon kérve.

## Poisoning with Generative approach (DeepConfuse)

A preturbáció kiszámításhoz egy $h_\xi$ neurális hálót használunk.

Alternálva frissítjük az eredeti háló ($f_\theta$) és a ($h_\xi$) súlyait.
Értelemszerűen a $\theta$ esetében gradiens sülyedést használunk, a $\xi$ esetében pedig gradiens emelkedést.

Az a baj, hogy ha csak naívan nekiesünk, akkor nem fog konvergálni.
Viszont a DeepConfuse ezt megoldja valami okos matekkal aminek az algoritmusát nem értem.

### MaxMin -> MinMax

Eddig az volt, hogy maximalizálni szerettük volna a poison-ök által hozzáadott hibát úgy, hogy közben a tanító + poison adaton minimális a hiba.

Helyette most kívül van az, hogy minimalizáljuk a tanító + poison adaton a hibát, és belül maximalizáljuk a poison-ok által hozzáadott hibát.

Ez azért lesz nekünk jó, mert valami miatt így gyorsabb lesz.

## Black-box Poisoning (Transferability)

Ha azt szeretnénk, hogy jól transzferálódjon a támadás, akkor ne válasszunk túl komplex modellt.

# Poisoning: Targeted

## Feature-collision

### Transfer learning esetében

A cél az, hogy a befagyasztott feature extractoron átltal adott feature-ökön a támadó minta nagyon közel kerüljön a cél mintához.

### End-to-end learning esetében

Nem működnek az eddigiek, mert a feature-extractor alkalmazkodik a támadó mintához.

Úgyhogy nem csak egy, hanem több támadó mintát adunk hozzá, és vízjelezünk.
Ezzel viszont sajnos az a baj, hogy ha ránéz az ember a képre, akkor elégg észrevehető a változás.

## Convex Polytope Attack (CPA)

Feature-collision esetében többet számít, hogy milyen irányból van a minta a cél mintához képest, mint az, hogy milyen messze van tőle.
Úgyhogy ennél a támádasánál olyan poison mintákat csinálunk, amik egy konvex sokszöget alkotnak a feature térben a cél minta körül.

A CPA még annyival csinál többet, hogy nem egy, hanem $m$ darab feature-extractort csinál, mindegyikhez csinál egy konvex sokszöget úgy, hogy mindegyikben benne legyen a cél minta.
Ezáltal sokkal jobban transzferálható lesz a támadás.

Van hozzá egy jó bonyolult matekos formalizáció amit nem értek.

Dropout segítségével könnyen előállítható az $m$ darab feature-extractor.

Van még egy adag matek, ami biztosan jó valamire.

És, amúgy még létezik egy Multi-layer CPA is, ami elvileg még többet tud, de itt már azt sem értem, hogy miben tud többet.

Amúgy a CPA egy nagyon jó támadás, mert:
- Lassú
- Nem olyan jól transzferálható
- És a cél mintának csak egy konkrét variációjára működik (egy konkrét STOP tábla fotóra és nem az összesre).

## Bullseye Polytope Attack

A matekot nem értem.

De egészen hasonlónak tűnik a CPA-hoz, csak azt írják róla, hogy jobban transzferálható, meg gyorsabban számolható.

## Witches' Brew Attack

1.  Kiválasztunk egy cél mintát $x_t$ , és meghatározzuk, hogy mire szeretnénk félreklasszifikálni $y_t^{adv}$.
2.  Ezután a tanítóhalmazból keresünk egy csomó olyan $x_i$ tanítóadatot, akiknek az $y_i$ címkéje megegyezik azzal amire félre szeretnénk osztályozni ($y_t^{adv}$).
3.  Azt szeretnénk elérni, hogy a kiválasztott $x_i+\delta_i$ perturbált tanítóadatok gradiense együttesen kiadja a az $x_t$ gradiensét.
    Ennek méréséhez koszinusz hasonlóságot használunk.
4.  Az egészet megcsinálhatuk több különböző modellre, és vehetjünk annak az átlagát.

## StingRay Attack

Kaptunk egy peszudokódot, hogy hajrá, értsük meg! (semmit sem értek)

## Subpopulation Attack

Stealthy.

# Poisoning: Backdoors

## LIRA

A lényeg, hogy odaadjuk valakinek az adatunkat, hogy tanítsa be rajta a modellt.
Ő a támadó, aki majd poison-öket akar elhelyezni.

Tanítás során minimalizálni akarja a rendes loss-t: $loss_f(\theta,x,y)$.
Illetve még a poison-öltet is: $loss_f(\theta,T_{\xi^*}(x),y_t)$.
Itt az a lényeg, hogy a $T_{\xi^*}$ függvény hozzáadja a triggert a bemenethez.

## Sleeper Agent (backdooring with gradient alignment)

Nem értem, hogy mi történik itt, de nagyon hasonlít a Witches' Brew-ra.

# Poisoning: Defenses

## RONI

- Tanít egy modellt úgy, hogy benne van a tanítóminta, megy úgy is, hogy nem.
- Megnézi, hogy mennyire volt negatív hatással a validációs halmazra, és ha egy egy $\tau$ jobban romlott, akkor azt mondja, hogy az egy poison.
- Ezt megcsinálja az összes mintára, és kész is.

Az a baj, hogy minden egyes mintára tanítani két modellt elképesztően drága.

## tRONI

Nem értem.

## SEVER

Ezt sem értem.

De állítólag jobban működik untargeted esetben.

## k-NN

- Minden mintára megnézi, hogy a k legközelebbi szomszédjának a címkéjét.
- Ha több mint a fele nem egyezik meg vele, akkor azt mondja rá, hogy poison.

Ha k nagyobb mint kétszer a posion-ök száma, akkor a poison-ök nem tudnak többségben lenni.

Ökölszabályként a k értéket a legnagyobb elemszámú osztály méretének 20%-ára állítjuk.

## Iterative Trimmed Loss

Kiszűri a túl nagy kezdeti loss-szal rendelkező tanítóadatokat.
A maradékon tanítja tovább a modellt.

## Data Augmentation

### Mixup

- Kiválaszt két random tanítómintát.
- Generál egy random $\lambda$ súly, aminek Dirichlet(1,1) eloszlása van (fogalmam sincsen, hogy mi az).
- Majd veszi a két tanítómintát és azok címkéjét, és ezeket összeadja súlyozottan.

### CutMix

- Az y címkéket ugyanúgy adja össze mint a Mixup.
- Az x bemeneteket azonban úgy állítja elő, hogy az egyik tanító mintából kívág egy darabot és azt ráilleszti a másikra.

## Adversarial Training

Ugyanaz mint evasion ellen, plusz még van egy adag érthetetlen matek amitől még jobb lesz elvileg.

## Differential Privacy

Semmit sem értek.

## Pointwise Provable Defense

Az ábráról annyi jön le, hogy a tanítóadatot szétosztjuk és több modellt tanítunk vele.
Ezen kívül ötletem sincsen, hogy mi történik.

## Deep Partition Aggregation (DPA)

Ez lehet, hogy ugyanaz akar lenni mint az előző.

Mindenesetre az a lényege, hogy a k modell mindegyikén kiértékelünk a teszt fázisban, és azt a címkét adjuk vissza amit a legtöbben mondanak.

# Poisoning: Defenses against backdoors

## Backdoor detection

### Activation Clustering

- Fogjuk az utolsó rejtett réteget.
- Dimenzi redukciót alkalmazunk rá (pl. PCA).
- Erre még ráküldünk egy k-means k=2-vel, ami elvileg szétválasztja a poison és nem poison mintákat, és a kisebbre mondjuka azt, hogy poison.
- Ezt az egészet megismételjük az összes osztályra.

Ez egy tök jó módszer, mert egyszerű.
Viszont az a baj vele, hogy nem működik a feature-alapú backdoor támadások ellen.

### Anomaly detection with spectral signatures

Fogalam sincsen, hogy mi történik, de a lényeg, hogy ez egy elméletileg bizonított megoldás, és jobb mint a Acrivation Clustering.

### STRIP

Matek...

### Neural Cleanse (trigger recovery)

- Fogunk egy címkét, és úgy gondolunk rá, mintha ez lenne a támadó célpontja.
- Minden lehetséges bemenetre kiszámoljuk, hogy mi az a minimális módosítás, amivel elérhető a cél címke.
- Ezt a két lépés megismételjük az összes címkére.
- Majd outlier detekciót csinálunk, ami valamire biztos jó lesz.

Ezután még van valami bonyolult matekos trigger reconstruction...

### MNTD: Meta Neural Trojan Detection

Csinálunk egy meta osztályozót, aminek a bemenete a modell, a kimenete pedig az, hogy van-e benne backdoor vagy nincs.

## Backdoor removal

### Fine-tuning

Az az ötlet, hogy a validációs halmazon tovább tanítjuk a modellt.
Sajnos azonban ez lehet, hogy nem fog működni.

Úgyhogy, használjuk valamelyik korábbi megoldást, hogy megkeressünk egy triggert, majd a validációt halmaz képeihez is adjuk hozzá a triggert, viszont itt a helyes címkét adjuk meg neki.
Ha így tanítunk tovább, akkor azt tanítjuk meg a modellnek, hogy miképp kell osztályoznia a triggerrel ellátott bemenetet helyesen.

### Pruning

Átküldjü a validációs halmazt a hálón.
Megnézzük, hogy melyik neuronok azok amik nem nagyon voltak aktiválva, és ezeket kivesszük.
Az, hogy mennyit veszünk ki attól függ, hogy mekkora romlást engedünk.

### Fine-pruning

A probléma a sima pruninggal az, hogy főleg a később rétegekből szűr ki neuronokat.
Erre felkészülhet a támadó, hogy ő is alkalmaz prunigot, és utána újra tanít.
(A végén természetesen az eredeti architektúrájú hálót kell visszaadja, de ott csak visszarakja hozzá azokat a neuronokat, amiket kivett a pruning során.)

Úgyhogy a fine-pruning azt csinálja, hogy először alkalmaz egy pruningot, majd utána azon még tovább tanít.

### Adversarial Neuron Pruning (ANP)

Nem kell tudni vizsgára.

### Neural Attention Distillation (NAD)

Szintén nem kell tudni vizsgára.

### Anti-Backdoor Learning (ABL)

És ezt sem kell tudni vizsgára.

### Mode Connectivity Repair (MCR)

Így is halál ez a tárgy, el sem tudom képzelni, ha még ez is kéne.

### Self-(semi)-supervised Learning against Backdoors

Nem is emlékszem, hogy lett volna ennyi előadáson, még jó, hogy ez sem kell.

# Availability

## Energy and Latency in Machine Learning

Egy csomó optimalizáció van, ami azt használja ki, hogy nullával való összeadás vagy szorzás van, mert nem teljesen sűrőek a mátrixok.
Ez nagyon király, viszont az a baj, hogy emiatt az átlagos eset és a lehető legrosszabb eset távolabb kerül egymástól.
Egy támadó célja értelemszerűan az, hogy megtalálja a lehető legrosszabb esetet.

## Sponge examples

- Fogunk egy random S bemeneti halmazt.
- Kiválasztunk k darab random párt, keresztezzük őket, majd az eredményt még mutáljuk.
- A legjobb (legtöbb energiát fogyasztó) 10%-ot kiválasztjuk, ez lesz az új S halmaz.
- Iterálunk amíg nem konvergál a maximális energia fogyasztás.

Az előbb leírt egy black-box támadás, de van white-box verziója is, aminek az a lényege, hogy nem válaszidő/felhasznált energia alapján nézzük a top 10%-ot, hanem azt nézzük, hogy konkértan mennyi neruon aktiválódik.

Ha pedig még lekérdezés szinjén sem férünk hozzá a támadni kívánt modellhez, akkor is lehet támadást csinálni, mert egy helyettesítő modellen tanított Sponge-t is lehet használni, azaz van transzferálhatóság.

## Sponge Data Poisoning

A támadó célja, hogy olyan modell készüljön, ami minden bemenetre sokat fogyaszt.
Ezt lehetőleg úgy akarja megtenni, hogy ne romoljon a modell pontossága, azaz ne vegyék észre.
Federált tanulás esetében kivitelezhető a legjobban.

A lényeg, hogy tanítás során van egy plusz loss is, ami azért felelős, hogy minél több neuron legyen aktiválva.
Azt viszont nem értem, hogy miért kell külön poison, meg sima adathalmaz, miért nem lehet csak simán a rendes halmazra számolni a rendes és az energiafogyasztós loss-t?

Amúgy érdekesség, hogyha megfordítjuk az energiáért felelős tag előjelét, akkor tudunk olyan hálót tanítani ami direkt minél kevesebbet fogyaszt.

## Sponge examples against Dynamic (Multi-exit) Neural Nets

Olyan támadómintákat gyártunk, amiknél bizonytalanok lesznek a háló korai döntést hozó részei, ezért további rétegek is aktiválódnak.

## Sponge Backdoors against Dynamic NN

Az előző támadással az a baj, hogy rontja a model általános pontosságát, ezáltal észrevehető.

Úgyhogy most olyan modellt akarunk tanítani, ami csak a triggert tartalmazó bemenetekre fogyaszt sok energiát.

## Sponge Images against Vision-Language Models

Ez szerencsére nem kell a vizsgára.

# Confidentiality: Privacy-attacks

## Model Inversion

A cél, hogy megszerezzük egy adott címkéhez tartozó átlagos bemenetet.

Gradiens süllyedünk, csak nem a modell, hanem a bemenet szerint.

Fontos, hogy nem rekonstruáltunk egy konkrét tanító mintát, és azt sem mondjuk meg, hogy egy adott minta volt-e használva a tanításhoz.

## Membership Inference

Itt az a cél, hogy megtudjuk, hogy egy adott minta volt-e használva a tanításhoz.

### Score-based

Ha a confidence score magas, akkor benne volt, egyébként nem.

Még jobban járunk ha confidence helyett loss-t használunk score függvényként.

### White-box

Következő ötlet, hogy loss helyett használjuk a loss gradiensét, mert az nem egy skalár, hanem egy vektor.
A baj csak az, hogy ez egy túl nagy vektor, hiszen a mérete megegyezik a modell méretével.

De, taníthatunk egy másik modellt, aminek többek között odaadjuk az eredeti gradiensét, és akkor majd az megmondja, hogy benne volt-e a minta vagy sem.
Ehhez persze kell, hogy legyenek biztos adataink, amikről tudjuk, hogy benne voltak, vagy nem voltak az eredeti adatban.

Ha ilyen adathalmaz nem áll a rendelhezésünkre, akkor csinálhatunk egy encoder-decoder hálót, aminek az encoder kimenetét ha klaszterezzük két osztályra.
Az az osztály fog a "nem volt benne" osztályhoz tartozni, amelyiknek nagyobb a gradiens normája.
Arról már fogalmam sincsen, hogy mit jelent a gradiens norma.

### Black-box

- Valahogyan szerzünk egy D' ugyanolyan eloszlású adathalmazt, mint amilyenen a kérdéses modellt tanították.
- Ezt az adathalmazt szétosztjuk k részre, majd még azokat is tanító és teszt halmazra.
- Betanítok k darab "shadow" modellt a tanító részeken.
- A D' adathalmaz minden elemét átalakítom tanítóadattá úgy, hogy a bemenet lesz az elvárt kimenet és a rá adott konfidenciák, a kimenet pedig az, hogy volt-e használva tanításhoz vagy sem.
- Az így létrejött adathalmazon tanítok egy modellt.
- Ezután végzek egy lekérdezést a támadni kívánt modell felé a kérdéses adattal.
- Majd az így szerzett választ bedobom a saját modellembe.

### Label-only

A támadó nem ismeri döntés konfidenciáját, csak a címkét.

- Fogunk egy bemenetet és alkalmazunk rá egy adag random transzformációt, így kapva egy halmaznyi össze-vissza transzformált bemenetet.
- Az így kapott halmaz minden elemére megnézzük, hogy eltalálta-e a model vagy sem, így kapva egy halmaznyi 0-1 címkét.
- Erre tanítunk egy modellt, ami a halmaznyi 0-1 címka alapján megmondja megmondja, hogy benne volt-e a minta a tanítóadatban vagy sem.

Ehhez persze kell, hogy legyen egy csomó olyan mintánk amiről tudjuk, hogy használva voltak-e a támadni kívánt modell tanításához.

Ha nincsen ilyen adatunk, akkor második megoldásként el lehet sütni a "shadow" modelles trükköt.

Egy harmadik megoldás lehet, hogy a konfidenciát becsüljük a döntési határtól való távolságtól, amit meg tudunk azzal becsülni, hogy mennyire kell módosítani egy mintát, hogy átkerüljön egy másik osztályba.
Amennyiben ez a távolság nagyobb mint egy tau határérték, akkor azt mondjuk, hogy member, egyébként nem.

### Likelihood Ratio Attack (LiRA)

Tanítunk egy csomó modellt, ezeknek lesz egy eloszlása.

Valószínűség...
Matek...
Valami...
Megint matek...
Loss...
Még több matek...

A lényeg, hogy ez ez szupi támadás, de nagyon dárga, mert mindegy egyes mintához egy csomó modellt kell tanitani.

## Attribute Inference

Innentől kezdve elegem van az egész tárgyból, már napok óta csak ezt tanulom.
Csak eleget tudok már egy ketteshez.

## Property Inference

# Confidentiality: Privacy-attack Defenses

## Differentially Private Gradient Descent

## Ad hoc Defenses

# Confidentiality: Model Extraction

# Confidentiality: Model Watermarking
