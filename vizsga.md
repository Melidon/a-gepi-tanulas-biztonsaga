# A gépi tanulás biztonsága - I. vizsga (VIHIMB09)

2024\. június 5.

1.  Mi a különbség a PGD (Projected Gradiens Descent) és a Carlini-Wagner evasion támadások között által megoldott optimalizációs problémák között?
    (3 pont)

2.  Hogyan használható az iránymenti derivált (directional derivative) hatékony gradiensbecslésre black-box evasion támadás esetén?
    Javasoljon egy black-box módszert az iránymenti derivált becslésére!
    (4 pont)

3.  Miért robosztusabbak az ensemble modellek evasion támadások ellen?
    Javasoljon egy támasást ensemble modellek ellen!
    (4 pont)

4.  Formalizálja a Witches' Brew (WiB) célzott poisioning támadást!
    Hogyan lehet a támadást optimalizációs problémaként megfogalmazni?
    (4 pont)

5.  Hogyan konstruálhato black-box alapú sponge example genetikus algoritmussal?
    (3 pont)

6.  Adott 10^5 darab tanító minta és egy ezen betanított konfidencia értékeket visszaadó black-box hozzáférésű modell.
    Hogyan határozható meg az a tanító minta, amire a Lira (Likelihood Ratio Atack) membership támadás a legpontosabb (worst-case privacy), ha legfeljebb 200 shadow modell tanítható?
    (4 pont)

7.  Hogyan használható az aktív tanulás modellopás céljára?
    Mondjon példáta aktív tanulás alapú modellopásra!
    (4 pont)
